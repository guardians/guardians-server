'use strict';
const sinon = require('sinon');
const sinonHelpers = require('sinon-helpers');
const rewire = require('rewire');
const assert = require('chai').assert;

const PathModel = require('../../../models/Path');
const UpdatePathService = rewire('../../../services/Path/UpdatePathService');

describe('UpdatePathService', () => {
    let PathModelMock,
        FSMock;

    beforeEach(() => {
        PathModelMock = sinonHelpers.getStubConstructor(PathModel.prototype);
        FSMock = sinon.stub();
        UpdatePathService.__set__({
            'PathModel': PathModelMock,
            'fs': FSMock
        });
    });

    describe('#update()', () => {
        beforeEach(() => {
            FSMock.write = sinon.stub(FSMock, "write")
                .returns(Promise.resolve({}));
        });

        it('refuses to update a file without client', () => {
            PathModelMock.findOne = sinon.stub().returns(Promise.resolve(null));

            return UpdatePathService.update({}, 'filepath')
                .then(assert.fail)
                .catch(err => {
                    assert.isTrue(PathModelMock.findOne.notCalled);
                    assert.isTrue(FSMock.write.notCalled);
                    assert.equal(err, 'GS-PROPERTIES');
                });
        });

        it('refuses to update a file without filepath', () => {
            PathModelMock.findOne = sinon.stub().returns(Promise.resolve(null));

            return UpdatePathService.update({}, undefined, {})
                .then(assert.fail)
                .catch(err => {
                    assert.isTrue(PathModelMock.findOne.notCalled);
                    assert.isTrue(FSMock.write.notCalled);
                    assert.equal(err, 'GS-PROPERTIES');
                });
        });

        it('refuses to update a non existing file', () => {
            PathModelMock.findOne = sinon.stub().resolves(null);

            return UpdatePathService.update({}, 'filepath', {username: 'test'})
                .then(() => assert.isTrue(false, "should refuse to update a new file"))
                .catch(error => {
                    assert.isTrue(PathModelMock.findOne.calledOnce);
                    assert.isTrue(FSMock.write.notCalled);
                    assert.equal(error, 'GS-NORESULT');
                });
        });

        it('accepts to update an existing file', () => {
            PathModelMock.findOne = sinon.stub()
                .resolves('RESULT');

            return UpdatePathService.update({data: "some content"}, 'filepath', {username: 'test'})
                .then(result => {
                    assert.isTrue(PathModelMock.findOne.calledOnce);
                    assert.isTrue(FSMock.write.calledOnce);
                    assert.isTrue(FSMock.write.calledWith('/tmp/latest/test/filepath', "some content"));
                    assert.equal(result, 'RESULT');
                });
        });
    });
});