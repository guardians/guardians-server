'use strict';
const sinon = require('sinon');
const sinonHelpers = require('sinon-helpers');
const rewire = require('rewire');
const assert = require('chai').assert;

const PathModel = require('../../../models/Path');
const RemovePathService = rewire('../../../services/Path/RemovePathService');

describe('RemovePathService', () => {
    let PathModelMock,
        FSPathServiceMock,
        VersionPathServiceMock,
        FSMock;

    beforeEach(() => {
        PathModelMock = sinonHelpers.getStubConstructor(PathModel.prototype);
        FSMock = sinon.stub();
        FSPathServiceMock = sinon.stub();
        VersionPathServiceMock = sinon.stub();

        RemovePathService.__set__({
            'PathModel': PathModelMock,
            'FSPathService': FSPathServiceMock,
            'VersionPathService': VersionPathServiceMock,
            'fs': FSMock
        });
    });

    describe('#remove()', () => {
        beforeEach(() => {
            PathModelMock.deletePath = sinon.stub().resolves();
            FSPathServiceMock.getPath = sinon.stub().returns('getPath');
        });

        it('refuses to remove a file without client', () => {
            PathModelMock.findOne = sinon.stub().resolves(null);
            FSMock.removeTree = sinon.stub().resolves({});

            return RemovePathService.remove(undefined, 'filepath')
                .then(assert.fail)
                .catch(err => {
                    assert.isTrue(PathModelMock.findOne.notCalled);
                    assert.isTrue(FSMock.removeTree.notCalled);
                    assert.equal(err, 'GS-PROPERTIES');
                });
        });

        it('refuses to remove a file without filepath', () => {
            PathModelMock.findOne = sinon.stub().resolves(null);
            FSMock.removeTree = sinon.stub().resolves({});

            return RemovePathService.remove({}, undefined)
                .then(assert.fail)
                .catch(err => {
                    assert.isTrue(PathModelMock.findOne.notCalled);
                    assert.isTrue(FSMock.removeTree.notCalled);
                    assert.equal(err, 'GS-PROPERTIES');
                });
        });

        it('refuses to remove an non existing file', () => {
            FSMock.isDirectory = sinon.stub().resolves(true);
            PathModelMock.findPath = sinon.stub().resolves(null);
            PathModelMock.deletePath = sinon.stub().resolves({deletedCount: 0});
            FSMock.removeTree = sinon.stub().resolves({});

            let client = {username: 'test'};

            return RemovePathService.remove('filepath', client)
                .then(() => assert.isTrue(false, "should refuse to remove a non existing file"))
                .catch(error => {
                    assert.isTrue(PathModelMock.findPath.calledOnce);
                    assert.deepEqual(PathModelMock.findPath.firstCall.args, ['filepath', client]);
                    assert.isTrue(PathModelMock.deletePath.notCalled);
                    assert.isTrue(FSMock.removeTree.notCalled);
                    assert.equal(error, 'GS-NORESULT');
                });
        });

        it('accepts to remove an existing file', () => {
            FSMock.isDirectory = sinon.stub().resolves(false);
            let pathModel = {path: 'filepath'};
            PathModelMock.findPath = sinon.stub().resolves(pathModel);
            PathModelMock.deletePath = sinon.stub().resolves({deletedCount: 1});
            FSMock.removeTree = sinon.stub().resolves({});
            VersionPathServiceMock.deleteVersions = sinon.stub().resolves();

            let client = {_id: 'userid', username: 'test'};
            return RemovePathService.remove('filepath', client)
                .then(result => {
                    assert.isTrue(PathModelMock.deletePath.calledOnce);
                    assert.deepEqual(PathModelMock.deletePath.firstCall.args, ['filepath', client]);
                    assert.isTrue(VersionPathServiceMock.deleteVersions.calledOnce);
                    assert.deepEqual(VersionPathServiceMock.deleteVersions.firstCall.args, [pathModel, client, null]);
                    assert.isTrue(FSMock.removeTree.calledOnce);
                    assert.isTrue(FSMock.removeTree.calledWith('getPath'));
                    assert.deepEqual(result, {deletedCount: 1});
                });
        });

        it('refuses to remove an non existing folder', () => {
            FSMock.isDirectory = sinon.stub().resolves(false);
            PathModelMock.findPath = sinon.stub().resolves(null);
            PathModelMock.deletePath = sinon.stub().resolves({deletedCount: 0});
            FSMock.removeTree = sinon.stub().resolves({});

            return RemovePathService.remove('folderpath', {username: 'test'})
                .then(() => assert.isTrue(false, "should refuse to remove a non existing file"))
                .catch(error => {
                    assert.isTrue(PathModelMock.deletePath.notCalled);
                    assert.isTrue(FSMock.removeTree.notCalled);
                    assert.equal(error, 'GS-NORESULT');
                });
        });

        it('accepts to remove an existing folder', () => {
            let client = {_id: 'userid', username: 'test'};
            let folderContent = {path: 'folderContent'};

            FSMock.isDirectory = sinon.stub().resolves(true);
            PathModelMock.findPath = sinon.stub().resolves({path: 'folderPath'});
            PathModelMock.findRecursiveFolderContent = sinon.stub()
                .resolves(folderContent);
            PathModelMock.deletePath = sinon.stub().resolves({deletedCount: 1});
            FSMock.removeTree = sinon.stub().resolves({});
            VersionPathServiceMock.deleteAllFilesVersions = sinon.stub().resolves();

            return RemovePathService.remove('folderPath', client)
                .then(result => {
                    assert.isTrue(PathModelMock.deletePath.calledOnce);
                    assert.deepEqual(PathModelMock.deletePath.firstCall.args, ['folderPath', client]);
                    assert.isTrue(VersionPathServiceMock.deleteAllFilesVersions.calledOnce);
                    assert.deepEqual(VersionPathServiceMock.deleteAllFilesVersions.firstCall.args, [folderContent, client]);
                    assert.isTrue(FSMock.removeTree.calledOnce);
                    assert.isTrue(FSMock.removeTree.calledWith('getPath'));
                    assert.deepEqual(result, {deletedCount: 1});
                });
        });
    });
});