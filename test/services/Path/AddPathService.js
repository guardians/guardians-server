'use strict';
const sinon = require('sinon');
const sinonHelpers = require('sinon-helpers');
const rewire = require('rewire');
const assert = require('chai').assert;

const PathModel = require('../../../models/Path');
const AddPathService = rewire('../../../services/Path/AddPathService');

describe('AddPathService', () => {
    let PathModelMock,
        FSMock;

    beforeEach(() => {
        PathModelMock = sinonHelpers.getStubConstructor(PathModel.prototype);
        FSMock = sinon.stub();
        AddPathService.__set__({
            'PathModel': PathModelMock,
            'fs': FSMock
        });
    });

    describe('#add()', () => {
        beforeEach(() => {
            FSMock.makeTree = sinon.stub(FSMock, "makeTree")
                .resolves({});
            FSMock.write = sinon.stub(FSMock, "write")
                .resolves({});
        });

        it('refuses to add a file without client', () => {
            return AddPathService.add({}, 'filepath')
                .then(assert.fail)
                .catch(err => {
                    assert.isEmpty(PathModelMock.getInstances());
                    assert.isTrue(FSMock.makeTree.notCalled);
                    assert.isTrue(FSMock.write.notCalled);
                    assert.equal(err, 'Path: file properties or Client are missing');
                });
        });

        it('refuses to add a file without filepath', () => {
            return AddPathService.add({}, undefined, {})
                .then(assert.fail)
                .catch(err => {
                    assert.isEmpty(PathModelMock.getInstances());
                    assert.isTrue(FSMock.makeTree.notCalled);
                    assert.isTrue(FSMock.write.notCalled);
                    assert.equal(err, 'Path: file properties or Client are missing');
                });
        });

        it('accepts to add a new file', () => {
            PathModelMock.withMethods('save', sinonHelpers.returning(Promise.resolve('RESULT')));

            return AddPathService.add({data: "some file"}, 'filepath', {username: 'test'})
                .then(result => {
                    assert.isTrue(PathModelMock.getInstance().save.calledOnce);
                    assert.isTrue(FSMock.makeTree.calledOnce);
                    assert.isTrue(FSMock.makeTree.calledWith('/tmp/latest/test'));
                    assert.isTrue(FSMock.write.calledOnce);
                    assert.isTrue(FSMock.write.calledWith('/tmp/latest/test/filepath', "some file"));
                    assert.equal(result, 'RESULT');
                });
        });

        it('refuses to add an existing file', () => {
            PathModelMock.withMethods('save', sinonHelpers.returning(Promise.reject('ERROR')));

            return AddPathService.add({}, 'filepath', {username: 'test'})
                .then(assert.fail)
                .catch(error => {
                    assert.isTrue(PathModelMock.getInstance().save.calledOnce);
                    assert.isTrue(FSMock.makeTree.notCalled);
                    assert.isTrue(FSMock.write.notCalled);
                    assert.equal(error, 'ERROR');
                });
        });
    });
});