'use strict';

'use strict';
const sinon = require('sinon');
const sinonHelpers = require('sinon-helpers');
const rewire = require('rewire');
const assert = require('chai').assert;
const PathModel = require('../../../models/Path');
const FindPathService = rewire('../../../services/Path/FindPathService');


describe('FindPathService', () => {
    let PathModelMock,
        FSMock,
        NodeFSMock,
        TargzMock;

    beforeEach(() => {
        PathModelMock = sinonHelpers.getStubConstructor(PathModel.prototype);
        TargzMock = sinon.stub();
        let targzConstructor = sinon.stub()
            .returns(TargzMock);
        FSMock = sinon.stub();
        NodeFSMock = sinon.stub();
        FindPathService.__set__({
            'PathModel': PathModelMock,
            'fs': FSMock,
            'nodeFS': NodeFSMock,
            'targz': targzConstructor
        });
    });

    describe("#findContent()", () => {
        it('refuses to download a file without client', () => {
            PathModelMock.findDirectFolderContent = sinon.stub();

            return FindPathService.findContent('filepath', null)
                .then(assert.fail)
                .catch(err => {
                    assert.isEmpty(PathModelMock.getInstances());
                    assert.isTrue(PathModelMock.findDirectFolderContent.notCalled);
                    assert.equal(err, 'Path: file properties or Client are missing');
                });
        });

        it("returns empty content if the path doesn't exist", () => {
            PathModelMock.findDirectFolderContent = sinon.stub()
                .resolves([]);

            return FindPathService.findContent('filepath', {username: 'test'})
                .then(actual => {
                    assert.isTrue(PathModelMock.findDirectFolderContent.calledOnce);
                    assert.deepEqual(PathModelMock.findDirectFolderContent.firstCall.args, ['/filepath/', {username: 'test'}]);
                    assert.isEmpty(actual);
                });
        });

        it("finds the content of the root folder", () => {
            let expected = ['expected'];
            PathModelMock.findDirectFolderContent = sinon.stub()
                .resolves(expected);
            let client = {username: 'test'};

            return FindPathService.findContent('', client)
                .then(actual => {
                    assert.isTrue(PathModelMock.findDirectFolderContent.calledOnce);
                    assert.isTrue(PathModelMock.findDirectFolderContent.calledWith('/', client));
                    assert.deepEqual(actual, expected);
                });
        });

        it("finds the content of the provided folder", () => {
            let expected = ['expected'];
            PathModelMock.findDirectFolderContent = sinon.stub()
                .resolves(expected);
            let client = {username: 'test'};
            return FindPathService.findContent('folderToFind', client)
                .then(actual => {
                    assert.isTrue(PathModelMock.findDirectFolderContent.calledOnce);
                    assert.isTrue(PathModelMock.findDirectFolderContent.calledWith('/folderToFind/', client));
                    assert.deepEqual(actual, expected);
                });
        });
    });

    describe("#download()", () => {
        it('refuses to download a file without client', () => {
            PathModelMock.findOne = sinon.stub();

            return FindPathService.download('filepath', null)
                .then(assert.fail)
                .catch(err => {
                    assert.isEmpty(PathModelMock.getInstances());
                    assert.isTrue(PathModelMock.findOne.notCalled);
                    assert.equal(err, 'Path: file properties or Client are missing');
                });
        });

        it('refuses to download a file without filepath', () => {
            PathModelMock.findOne = sinon.stub();

            return FindPathService.download({username: 'test'}, undefined)
                .then(assert.fail)
                .catch(err => {
                    assert.isEmpty(PathModelMock.getInstances());
                    assert.isTrue(PathModelMock.findOne.notCalled);
                    assert.equal(err, 'Path: file properties or Client are missing');
                });
        });

        it("rejects if the path to download doesn't exist", () => {
            PathModelMock.findPath = sinon.stub()
                .resolves(null);

            return FindPathService.download('filepath', {username: 'test'})
                .then(assert.fail)
                .catch(err => {
                    assert.isTrue(PathModelMock.findPath.calledOnce);
                    assert.equal(err, 'GS-NORESULT');
                });
        });

        it("lets download the latest version of a file", () => {
            let expected = {
                path: "/tmp/latest/test/filepath",
                mimetype: 'application/json',
                content: "Pretend to be a stream"
            };

            PathModelMock.findPath = sinon.stub()
                .resolves({
                    _id: 'filepath',
                    mimetype: expected.mimetype
                });

            FSMock.isDirectory = sinon.stub()
                .resolves(false);
            NodeFSMock.createReadStream = sinon.stub()
                .returns(expected.content);

            return FindPathService.download('filepath', {username: 'test'})
                .then(res => {
                    assert.isTrue(PathModelMock.findPath.calledOnce);
                    assert.isTrue(FSMock.isDirectory.calledOnce);
                    assert.equal(FSMock.isDirectory.firstCall.args[0], expected.path);
                    assert.isTrue(NodeFSMock.createReadStream.calledOnce);
                    assert.isTrue(NodeFSMock.createReadStream.calledWith(expected.path));
                    assert.deepEqual(res, expected);
                });
        });

        it("lets download an older version of a file", () => {
            let expected = {
                path: "/tmp/versions/3/test/filepath",
                mimetype: 'application/json',
                content: "Pretend to be a stream"
            };

            PathModelMock.findPath = sinon.stub()
                .resolves({
                    _id: 'filepath',
                    mimetype: expected.mimetype
                });

            FSMock.isDirectory = sinon.stub()
                .resolves(false);
            NodeFSMock.createReadStream = sinon.stub()
                .returns(expected.content);

            return FindPathService.download('filepath', {username: 'test'}, 3)
                .then(res => {
                    assert.isTrue(PathModelMock.findPath.calledOnce);
                    assert.isTrue(FSMock.isDirectory.calledWith(expected.path));
                    assert.isTrue(NodeFSMock.createReadStream.calledOnce);
                    assert.isTrue(NodeFSMock.createReadStream.calledWith(expected.path));
                    assert.deepEqual(res, expected);
                });
        });

        it("lets download the latest version of a folder", () => {
            let expected = {
                path: "/tmp/latest/test/folderpath",
                mimetype: 'application/tar+gzip',
                content: "Pretend to be a stream"
            };

            PathModelMock.findPath = sinon.stub()
                .resolves({
                    _id: 'folderpath/myjsfile',
                    mimetype: 'application/json'
                });

            FSMock.isDirectory = sinon.stub()
                .resolves(true);
            TargzMock.createReadStream = sinon.stub()
                .returns(expected.content);

            const client = {username: 'test'};
            return FindPathService.download('folderpath', client)
                .then(res => {
                    assert.isTrue(PathModelMock.findPath.calledOnce);
                    assert.deepEqual(PathModelMock.findPath.getCall(0).args, ['folderpath', client, null]);
                    assert.isTrue(FSMock.isDirectory.calledOnce);
                    assert.isTrue(FSMock.isDirectory.calledWith(expected.path));
                    assert.isTrue(TargzMock.createReadStream.calledOnce);
                    assert.isTrue(TargzMock.createReadStream.calledWith(expected.path));
                    assert.deepEqual(res, expected);
                });
        });
    });
});