'use strict';
const sinon = require('sinon');
const sinonHelpers = require('sinon-helpers');
const rewire = require('rewire');
const assert = require('chai').assert;

const PathModel = require('../../../models/Path');
const VersionPathService = rewire('../../../services/Path/VersionPathService');

describe('VersionPathService', () => {
    let PathModelMock,
        FSPathServiceMock,
        FSMock,
        NodeFSMock;

    beforeEach(() => {
        PathModelMock = sinonHelpers.getStubConstructor(PathModel.prototype);
        FSPathServiceMock = sinon.stub();
        FSMock = sinon.stub();
        NodeFSMock = sinon.stub();
        VersionPathService.__set__({
            'PathModel': PathModelMock,
            'FSPathService': FSPathServiceMock,
            'fs': FSMock
        });
    });

    describe('#createVersion()', () => {
        beforeEach(() => {
            FSMock.makeTree = sinon.stub()
                .resolves();

            FSMock.write = sinon.stub()
                .resolves();

            FSMock.move = sinon.stub()
                .resolves();

            FSPathServiceMock.getPath = sinon.stub()
                .returns('getPath/latest/filepath');
        });

        it('refuses to versionize without client', () => {
            PathModelMock.findPath = sinon.stub().resolves(null);

            return VersionPathService.createVersion({}, 'filepath')
                .then(assert.fail)
                .catch(err => {
                    assert.isTrue(PathModelMock.findPath.notCalled);
                    assert.isTrue(FSMock.makeTree.notCalled);
                    assert.isTrue(FSMock.move.notCalled);
                    assert.isTrue(FSMock.write.notCalled);
                    assert.equal(err, 'GS-PROPERTIES');
                });
        });

        it('refuses to versionize without filepath', () => {
            PathModelMock.findPath = sinon.stub().resolves(null);

            return VersionPathService.createVersion({}, undefined, {})
                .then(assert.fail)
                .catch(err => {
                    assert.isTrue(PathModelMock.findPath.notCalled);
                    assert.isTrue(FSMock.makeTree.notCalled);
                    assert.isTrue(FSMock.move.notCalled);
                    assert.isTrue(FSMock.write.notCalled);
                    assert.equal(err, 'GS-PROPERTIES');
                });
        });

        it('refuses to versionize without file content', () => {
            PathModelMock.findPath = sinon.stub().resolves(null);

            return VersionPathService.createVersion(undefined, 'filepath', {})
                .then(assert.fail)
                .catch(err => {
                    assert.isTrue(PathModelMock.findPath.notCalled);
                    assert.isTrue(FSMock.makeTree.notCalled);
                    assert.isTrue(FSMock.move.notCalled);
                    assert.isTrue(FSMock.write.notCalled);
                    assert.equal(err, 'GS-PROPERTIES');
                });
        });

        it('refuses to create new version of a non existing file', () => {
            PathModelMock.findPath = sinon.stub().resolves(null);

            return VersionPathService.createVersion({}, 'filepath', {username: 'test'})
                .then(() => assert.isTrue(false, "should refuse to versionize a new file"))
                .catch(error => {
                    assert.isTrue(PathModelMock.findPath.calledOnce);
                    assert.isTrue(PathModelMock.findPath.calledWith('filepath', {username: 'test'}));
                    assert.isTrue(FSMock.makeTree.notCalled);
                    assert.isTrue(FSMock.move.notCalled);
                    assert.isTrue(FSMock.write.notCalled);
                    assert.equal(error, 'GS-NORESULT');
                });
        });

        it('accepts to create a new version of an existing file', () => {
            let filepath = 'filepath';
            let client = {username: 'test'};

            let pathModelFindMock = new PathModelMock();
            pathModelFindMock.versions = [{number: 0}, {number: 1}];

            pathModelFindMock.getNextVersionNumber = sinon.stub()
                .returns(0);
            pathModelFindMock.save = sinon.stub()
                .resolves();
            PathModelMock.findPath = sinon.stub()
                .resolves(pathModelFindMock);

            FSPathServiceMock.getPath.withArgs(filepath, client, 0)
                .returns('getPath/version/0/filepath');
            FSPathServiceMock.getPath.withArgs(filepath, client, 1)
                .returns('getPath/version/1/filepath');

            return VersionPathService.createVersion({data: "some content"}, filepath, client)
                .then(() => {
                    assert.isTrue(PathModelMock.findPath.calledOnce);
                    assert.isTrue(PathModelMock.findPath.calledWith('filepath', {username: 'test'}));
                    assert.isTrue(FSMock.makeTree.calledOnce);
                    assert.isTrue(FSMock.makeTree.calledWith('getPath/version/0'));
                    assert.isTrue(FSMock.move.calledOnce);
                    assert.isTrue(FSMock.move.calledWith('getPath/latest/filepath', 'getPath/version/0/filepath'));
                    assert.isTrue(FSMock.write.calledOnce);
                    assert.isTrue(FSMock.write.calledWith('getPath/latest/filepath', "some content"));
                });
        });

        it('keeps only {N} versions of the file, dependinf on `keep` parameter', () => {
            let filepath = 'filepath';
            let client = {username: 'test'};

            let pathModelFindMock = new PathModelMock();
            pathModelFindMock.path = filepath;
            pathModelFindMock.versions = [{number: 0}, {number: 1}];

            pathModelFindMock.getNextVersionNumber = sinon.stub()
                .returns(2);
            pathModelFindMock.save = sinon.stub()
                .resolves();
            PathModelMock.findPath = sinon.stub()
                .resolves(pathModelFindMock);

            FSPathServiceMock.getPath.withArgs(filepath, client, 0)
                .returns('getPath/version/0/filepath');
            FSPathServiceMock.getPath.withArgs(filepath, client, 1)
                .returns('getPath/version/1/filepath');
            FSPathServiceMock.getPath.withArgs(filepath, client, 2)
                .returns('getPath/version/2/filepath');

            FSMock.remove = sinon.stub().resolves();
            return VersionPathService.createVersion({data: "some content"}, filepath, client, 2)
                .then(result => {
                    assert.isTrue(PathModelMock.findPath.calledOnce);
                    assert.isTrue(PathModelMock.findPath.calledWith('filepath', {username: 'test'}));
                    assert.isTrue(FSMock.makeTree.calledOnce);
                    assert.isTrue(FSMock.makeTree.calledWith('getPath/version/2'));
                    assert.isTrue(FSMock.move.calledOnce);
                    assert.isTrue(FSMock.move.calledWith('getPath/latest/filepath', 'getPath/version/2/filepath'));
                    assert.isTrue(FSMock.write.calledOnce);
                    assert.isTrue(FSMock.write.calledWith('getPath/latest/filepath', "some content"));
                    assert.isTrue(pathModelFindMock.save.calledOnce);
                    assert.isTrue(FSMock.remove.calledOnce);
                    assert.isTrue(FSMock.remove.calledWith('getPath/version/0/filepath'));
                    assert.deepEqual(result.versions.map(v => v.number), [1, 2]);
                });
        });
    });

    describe('#deleteVersions()', () => {
        it('does nothing if there is no version of the provided file', () => {
            let pathModel = {versions: null};
            let client = {username: 'test'};

            FSMock.remove = sinon.stub();

            return VersionPathService.deleteVersions(pathModel, client, null)
                .then(() => {
                    assert.isTrue(FSMock.remove.notCalled);
                });
        });

        it('removes every versions of the provided file', () => {
            let pathModel = {
                path: 'filepath',
                versions: [{number: 0}, {number: 1}]
            };
            let client = {username: 'test'};

            FSMock.remove = sinon.stub();

            FSPathServiceMock.getPath = sinon.stub();
            FSPathServiceMock.getPath.withArgs(pathModel.path, client, 0)
                .returns('getPath/version/0');
            FSPathServiceMock.getPath.withArgs(pathModel.path, client, 1)
                .returns('getPath/version/1');

            pathModel.save = sinon.stub().resolves();

            return VersionPathService.deleteVersions(pathModel, client, null)
                .then(result => {
                    assert.isTrue(FSMock.remove.calledTwice);
                    assert.equal(FSMock.remove.firstCall.args[0], 'getPath/version/0');
                    assert.equal(FSMock.remove.secondCall.args[0], 'getPath/version/1');
                    assert.isEmpty(result.versions);
                });
        });

        it('doesn\'t remove versions if keep >= versions.length', () => {
            let pathModel = {
                path: 'filepath',
                versions: [{number: 0}, {number: 1}]
            };
            let client = {username: 'test'};

            FSMock.remove = sinon.stub();

            FSPathServiceMock.getPath = sinon.stub();
            FSPathServiceMock.getPath.withArgs(pathModel.path, client, 0)
                .returns('getPath/version/0');
            FSPathServiceMock.getPath.withArgs(pathModel.path, client, 1)
                .returns('getPath/version/1');

            pathModel.save = sinon.stub().resolves();

            return VersionPathService.deleteVersions(pathModel, client, 2)
                .then(result => {
                    assert.isTrue(FSMock.remove.notCalled);
                    assert.isTrue(pathModel.save.notCalled);
                    assert.deepEqual(result, pathModel);
                });
        });

        it('keeps the last versions depending on `keep` parameter', () => {
            let pathModel = {
                path: 'filepath',
                versions: [{number: 0}, {number: 1}, {number: 2}, {number: 3}]
            };
            let client = {username: 'test'};

            FSMock.remove = sinon.stub();

            FSPathServiceMock.getPath = sinon.stub();
            FSPathServiceMock.getPath.withArgs(pathModel.path, client, 0)
                .returns('getPath/version/0');
            FSPathServiceMock.getPath.withArgs(pathModel.path, client, 1)
                .returns('getPath/version/1');
            FSPathServiceMock.getPath.withArgs(pathModel.path, client, 2)
                .returns('getPath/version/2');
            FSPathServiceMock.getPath.withArgs(pathModel.path, client, 3)
                .returns('getPath/version/3');

            pathModel.save = sinon.stub().resolves();

            return VersionPathService.deleteVersions(pathModel, client, 2)
                .then(result => {
                    assert.isTrue(FSMock.remove.calledTwice);
                    assert.equal(FSMock.remove.firstCall.args[0], 'getPath/version/0');
                    assert.equal(FSMock.remove.secondCall.args[0], 'getPath/version/1');
                    assert.isFalse(FSMock.remove.calledWith('getPath/version/2'));
                    assert.isFalse(FSMock.remove.calledWith('getPath/version/3'));
                    assert.deepEqual(result.versions, [{number: 2}, {number: 3}]);
                });
        });

    });

    describe('#deleteAllFilesVersions()', () => {
        it('removes every versions of the provided files', () => {
            let pathModelArray = [
                {
                    path: 'filepath1',
                    versions: [{number: 0}, {number: 1}],
                    save: sinon.stub().resolves()
                },
                {
                    path: 'filepath2',
                    versions: [{number: 0}, {number: 1}],
                    save: sinon.stub().resolves()
                }];

            let client = {username: 'test'};

            FSMock.remove = sinon.stub();

            FSPathServiceMock.getPath = sinon.stub();
            FSPathServiceMock.getPath.withArgs(pathModelArray[0].path, client, 0)
                .returns('getPath1/version/0');
            FSPathServiceMock.getPath.withArgs(pathModelArray[0].path, client, 1)
                .returns('getPath1/version/1');
            FSPathServiceMock.getPath.withArgs(pathModelArray[1].path, client, 0)
                .returns('getPath2/version/0');
            FSPathServiceMock.getPath.withArgs(pathModelArray[1].path, client, 1)
                .returns('getPath2/version/1');

            return VersionPathService.deleteAllFilesVersions(pathModelArray, client)
                .then(() => {
                    assert.equal(FSMock.remove.callCount, 4);
                    assert.isTrue(FSMock.remove.calledWith('getPath1/version/0'));
                    assert.isTrue(FSMock.remove.calledWith('getPath1/version/1'));
                    assert.isTrue(FSMock.remove.calledWith('getPath2/version/0'));
                    assert.isTrue(FSMock.remove.calledWith('getPath2/version/1'));
                    assert.isEmpty(pathModelArray[0].versions);
                    assert.isTrue(pathModelArray[0].save.calledOnce);
                    assert.isEmpty(pathModelArray[1].versions);
                    assert.isTrue(pathModelArray[1].save.calledOnce);
                });
        });
    });

    describe('#removeVersions()', () => {
        beforeEach(() => {
            FSPathServiceMock.getPath = sinon.stub()
                .returns('getPath/latest/filepath');
        });

        it('refuses to delete versions without client', () => {
            PathModelMock.findPath = sinon.stub().resolves(null);

            return VersionPathService.removeVersions('filepath')
                .then(assert.fail)
                .catch(err => {
                    assert.isTrue(PathModelMock.findPath.notCalled);
                    assert.equal(err, 'GS-PROPERTIES');
                });
        });

        it('refuses to delete versions without filepath', () => {
            PathModelMock.findPath = sinon.stub().resolves(null);

            return VersionPathService.removeVersions(undefined, {username: 'test'})
                .then(assert.fail)
                .catch(err => {
                    assert.isTrue(PathModelMock.findPath.notCalled);
                    assert.equal(err, 'GS-PROPERTIES');
                });
        });

        it('refuses to delete versions of a non existing file', () => {
            PathModelMock.findPath = sinon.stub().resolves(null);
            FSMock.remove = sinon.stub().resolves();

            return VersionPathService.removeVersions('filepath', {username: 'test'})
                .then(() => assert.isTrue(false, "should refuse to versionize a new file"))
                .catch(error => {
                    assert.isTrue(PathModelMock.findPath.calledOnce);
                    assert.isTrue(PathModelMock.findPath.calledWith('filepath', {username: 'test'}));
                    assert.isTrue(FSMock.remove.notCalled);
                    assert.equal(error, 'GS-NORESULT');
                });
        });

        it('removes versions the provided file', () => {
            let client = {username: 'test'};
            let pathModel = {
                path: 'filepath',
                versions: [{number: 0}, {number: 1}]
            };

            PathModelMock.findPath = sinon.stub().resolves(pathModel);

            FSMock.remove = sinon.stub().resolves();
            FSPathServiceMock.getPath = sinon.stub();
            FSPathServiceMock.getPath.withArgs(pathModel.path, client, 0)
                .returns('getPath/version/0');
            FSPathServiceMock.getPath.withArgs(pathModel.path, client, 1)
                .returns('getPath/version/1');

            pathModel.save = sinon.stub().resolves();

            return VersionPathService.removeVersions('filepath', client)
                .then(() => {
                    assert.isTrue(PathModelMock.findPath.calledOnce);
                    assert.isTrue(PathModelMock.findPath.calledWith('filepath', {username: 'test'}));
                    assert.isTrue(FSMock.remove.calledTwice);
                    assert.isTrue(FSMock.remove.calledWith('getPath/version/0'));
                    assert.isTrue(FSMock.remove.calledWith('getPath/version/1'));
                    assert.isEmpty(pathModel.versions);
                    assert.isTrue(pathModel.save.called);
                    assert.isTrue(pathModel.save.calledOnce);
                });
        });
    });
});