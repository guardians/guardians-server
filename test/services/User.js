'use strict';
const path = require('path');
const sinon = require('sinon');
const sinonHelpers = require('sinon-helpers');
const rewire = require('rewire');
const assert = require('chai').assert;

const UserModel = require('../../models/User');
const UserTokenModel = require('../../models/UserToken');
const UserService = rewire('../../services/User');

describe('UserService', () => {
    let UserModelMock,
        UserTokenModelMock,
        utilsMock;

    beforeEach(() => {
        UserModelMock = sinonHelpers.getStubConstructor(UserModel.prototype);
        UserTokenModelMock = sinonHelpers.getStubConstructor(UserTokenModel.prototype);
        utilsMock = sinon.mock();

        UserService.__set__({
            'UserModel': UserModelMock,
            'UserTokenModel': UserTokenModelMock,
            'utils': utilsMock
        });
    });

    describe("#add()", () => {
        it('refuses to add a new User without name', () => {
            return UserService.add(null, 'fakePassword')
                .then(assert.fail)
                .catch(err => {
                    assert.isEmpty(UserModelMock.getInstances());
                    assert.isTrue(UserModelMock.save.notCalled);
                    assert.equal(err, 'User: User name or password not provided');
                });
        });

        it('refuses to add a new User without password', () => {
            return UserService.add('fakeUsername')
                .then(assert.fail)
                .catch(err => {
                    assert.isEmpty(UserModelMock.getInstances());
                    assert.isTrue(UserModelMock.save.notCalled);
                    assert.equal(err, 'User: User name or password not provided');
                });
        });

        it('correcly adds a new User', () => {
            UserModelMock.withMethods('save', sinonHelpers.returning(Promise.resolve('RESULT')));

            utilsMock.salt = sinon.stub()
                .returns('saltPassword');

            return UserService.add("fakeUsername", "fakePassword")
                .then(() => {
                    assert.equal(1, UserModelMock.getInstances().length);
                    assert.isTrue(UserModelMock.getInstance().save.calledOnce);
                    assert.isTrue(utilsMock.salt.calledWith('fakePassword'));
                });
        });
    });

    describe('#login()', () => {
        beforeEach(() => {
            UserModelMock.findOne = sinon.stub();
            utilsMock.salt = sinon.stub()
                .returns('saltPassword');
        });

        it('refuses to login a User without username', () => {
            return UserService.login(undefined, 'password')
                .then(assert.fail)
                .catch(err => {
                    assert.isTrue(UserModelMock.findOne.notCalled);
                    assert.equal(err, 'Please provide username/password');
                });
        });

        it('refuses to login a User without password', () => {
            return UserService.login('username', undefined)
                .then(assert.fail)
                .catch(err => {
                    assert.isTrue(UserModelMock.findOne.notCalled);
                    assert.equal(err, 'Please provide username/password');
                });
        });

        it('refuses to login an unknown User', () => {
            UserModelMock.findOne.returns(Promise.resolve(null));

            return UserService.login('username', 'password')
                .catch(err => {
                    assert.isTrue(utilsMock.salt.calledOnce);
                    assert.isTrue(utilsMock.salt.calledWith('password'));
                    assert.isTrue(UserModelMock.findOne.calledOnce);
                    assert.equal(err, 'Bad User username/password');
                });
        });

        it('correctly login the User', () => {
            let expected = {_id: 'ok'};
            UserModelMock.findOne.returns(Promise.resolve(expected));

            return UserService.login('username', 'password')
                .then(User => {
                    assert.isTrue(utilsMock.salt.calledOnce);
                    assert.isTrue(utilsMock.salt.calledWith('password'));
                    assert.isTrue(UserModelMock.findOne.calledOnce);
                    assert.equal(User, expected);
                });
        });
    });

    describe('#loginUsingToken()', () => {
        beforeEach(() => {
            UserTokenModelMock.findByToken = sinon.stub();

        });

        it('refuses to login a User without token', () => {
            return UserService.loginUsingToken(undefined)
                .then(assert.fail)
                .catch(err => {
                    assert.isTrue(UserTokenModelMock.findByToken.notCalled);
                    assert.equal(err, 'Please provide token');
                });
        });

        it('refuses to login an unknown User', () => {
            UserTokenModelMock.findByToken.resolves(null);

            return UserService.loginUsingToken('fake-token')
                .catch(err => {
                    assert.isTrue(UserTokenModelMock.findByToken.calledOnce);
                    assert.isTrue(UserTokenModelMock.findByToken.calledWith('fake-token'));
                    assert.equal(err, 'Bad User token');
                });
        });

        it('correctly login the User', () => {
            let expected = {_id: 'ok'};
            UserTokenModelMock.findByToken.resolves(expected);

            return UserService.loginUsingToken('fake-token')
                .then(User => {
                    assert.isTrue(UserTokenModelMock.findByToken.calledOnce);
                    assert.isTrue(UserTokenModelMock.findByToken.calledWith('fake-token'));
                    assert.equal(User, expected);
                });
        });
    });

    describe('#createToken()', () => {
        beforeEach(() => {
            UserTokenModelMock.findByToken = sinon.stub();
        });

        it('creates a new UserToken', () => {
            let user = {_id: "fakeUser"};
            let token = {
                token: "fake-user"
            };
            let expectedUser = {
                _id: "fakeUser",
                token: "fake-token",
                expires: "fake-expires"
            };

            UserTokenModelMock.createToken = sinon.stub()
                .resolves(token);
            UserTokenModelMock.findByToken = sinon.stub()
                .resolves(expectedUser);

            return UserService.createToken(user)
                .then(actual => {
                    assert.isTrue(UserTokenModelMock.createToken.calledOnce);
                    assert.isTrue(UserTokenModelMock.createToken.calledWith(user));
                    assert.isTrue(UserTokenModelMock.findByToken.calledOnce);
                    assert.isTrue(UserTokenModelMock.findByToken.calledWith(token.token));
                    assert.deepEqual(actual, expectedUser);
                });
        });
    });
});

