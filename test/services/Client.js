'use strict';
const path = require('path');
const sinon = require('sinon');
const sinonHelpers = require('sinon-helpers');
const rewire = require('rewire');
const assert = require('chai').assert;

const ClientModel = require('../../models/Client');
const ClientService = rewire('../../services/Client');

describe('ClientService', () => {
    let ClientModelMock,
        utilsMock;

    beforeEach(() => {
        ClientModelMock = sinonHelpers.getStubConstructor(ClientModel.prototype);
        utilsMock = sinon.mock();

        ClientService.__set__({
            'ClientModel': ClientModelMock,
            'utils': utilsMock
        });
    });

    describe('#login()', () => {
        it('refuses to list clients without users', () => {
            ClientModelMock.findByUser = sinon.stub();
            return ClientService.list(undefined)
                .then(assert.fail)
                .catch(err => {
                    assert.isTrue(ClientModelMock.findByUser.notCalled);
                    assert.equal(err, 'Client: User is not provided');
                });
        });

        it('finds clients of the provided user', () => {
            let user = {_id: 'fake-user'};
            let clients = [{_id: 'fake-client'}];

            ClientModelMock.findByUser = sinon.stub()
                .resolves(clients);

            return ClientService.list(user)
                .then(actual => {
                    assert.isTrue(ClientModelMock.findByUser.calledOnce);
                    assert.isTrue(ClientModelMock.findByUser.calledWith(user));
                    assert.deepEqual(actual, clients);
                });
        });
    });

    describe("#add()", () => {
        it('refuses to add a new Client without name', () => {
            return ClientService.add({password: 'fakePassword'}, {_id: 'fakeId'})
                .then(assert.fail)
                .catch(err => {
                    assert.isEmpty(ClientModelMock.getInstances());
                    assert.isTrue(ClientModelMock.save.notCalled);
                    assert.equal(err, 'Client: Client name or password not provided');
                });
        });

        it('refuses to add a new Client without password', () => {
            return ClientService.add({username: 'fakeUsername'}, {_id: 'fakeId'})
                .then(assert.fail)
                .catch(err => {
                    assert.isEmpty(ClientModelMock.getInstances());
                    assert.isTrue(ClientModelMock.save.notCalled);
                    assert.equal(err, 'Client: Client name or password not provided');
                });
        });

        it('refuses to add a new Client without User', () => {
            let newClient = {username: 'fakeUsername', password: 'fakePassword'};
            return ClientService.add(newClient, null)
                .then(assert.fail)
                .catch(err => {
                    assert.isEmpty(ClientModelMock.getInstances());
                    assert.isTrue(ClientModelMock.save.notCalled);
                    assert.equal(err, 'Client: User not provided');
                });
        });

        it('correcly adds a new Client', () => {
            ClientModelMock.withMethods('save', sinonHelpers.returning(Promise.resolve('RESULT')));

            let newClient = {username: 'fakeUsername', password: 'fakePassword'};
            let user = {_id: 'fakeUser'};
            utilsMock.salt = sinon.stub()
                .returns('saltPassword');

            return ClientService.add(newClient, user)
                .then(() => {
                    assert.equal(1, ClientModelMock.getInstances().length);
                    assert.isTrue(ClientModelMock.getInstance().save.calledOnce);
                    assert.isTrue(utilsMock.salt.calledWith('fakePassword'));
                });
        });
    });

    describe("#update()", () => {
        it('refuses to update a Client without name', () => {
            return ClientService.update({password: 'fakePassword'}, {_id: 'fakeId'})
                .then(assert.fail)
                .catch(err => {
                    assert.isEmpty(ClientModelMock.getInstances());
                    assert.isTrue(ClientModelMock.save.notCalled);
                    assert.equal(err, 'Client: Client name or password not provided');
                });
        });

        it('refuses to update a Client without password', () => {
            return ClientService.update({username: 'fakeUsername'}, {_id: 'fakeId'})
                .then(assert.fail)
                .catch(err => {
                    assert.isEmpty(ClientModelMock.getInstances());
                    assert.isTrue(ClientModelMock.save.notCalled);
                    assert.equal(err, 'Client: Client name or password not provided');
                });
        });

        it('refuses to update a Client which doesn\'t exist', () => {
            ClientModelMock.findOne = sinon.stub()
                .resolves(null);

            let client = {username: 'fakeUsername', password: 'fakePassword'};
            let user = {_id: 'fakeUser'};
            return ClientService.update(client, user)
                .then(assert.fail)
                .catch(err => {
                    assert.equal(err, 'This Client doesn\'t exist');
                });
        });

        it('correctly updates an existing Client', () => {
            utilsMock.salt = sinon.stub()
                .returns('saltPassword');

            let foundClientStub = sinon.stub();
            foundClientStub.update = sinon.stub()
                .resolves();
            ClientModelMock.findOne = sinon.stub()
                .resolves(foundClientStub);

            let newClient = {username: 'fakeUsername', password: 'fakePassword'};
            let user = {_id: 'fakeUser'};
            utilsMock.salt = sinon.stub()
                .returns('saltPassword');

            return ClientService.update(newClient, user)
                .then(() => {
                    assert.isTrue(ClientModelMock.findOne.calledOnce);
                    assert.isTrue(utilsMock.salt.calledWith('fakePassword'));
                    assert.isTrue(foundClientStub.update.calledOnce);
                    assert.isTrue(foundClientStub.update.calledWith({
                        'username': 'fakeUsername',
                        'password': 'saltPassword'
                    }));
                });
        });
    });

    describe('#login()', () => {
        beforeEach(() => {
            ClientModelMock.findOne = sinon.stub();
            utilsMock.salt = sinon.stub()
                .returns('saltPassword');
        });

        it('refuses to login a client without username', () => {
            return ClientService.login(undefined, 'password')
                .then(assert.fail)
                .catch(err => {
                    assert.isTrue(ClientModelMock.findOne.notCalled);
                    assert.equal(err, 'Please provide username/password');
                });
        });

        it('refuses to login a client without password', () => {
            return ClientService.login('username', undefined)
                .then(assert.fail)
                .catch(err => {
                    assert.isTrue(ClientModelMock.findOne.notCalled);
                    assert.equal(err, 'Please provide username/password');
                });
        });

        it('refuses to login an unknown client', () => {
            ClientModelMock.findOne.returns(Promise.resolve(null));

            return ClientService.login('username', 'password')
                .catch(err => {
                    assert.isTrue(utilsMock.salt.calledOnce);
                    assert.isTrue(utilsMock.salt.calledWith('password'));
                    assert.isTrue(ClientModelMock.findOne.calledOnce);
                    assert.equal(err, 'Bad Client username/password');
                });
        });

        it('correctly login the client', () => {
            let expected = {_id: 'ok'};
            ClientModelMock.findOne.returns(Promise.resolve(expected));

            return ClientService.login('username', 'password')
                .then(client => {
                    assert.isTrue(utilsMock.salt.calledOnce);
                    assert.isTrue(utilsMock.salt.calledWith('password'));
                    assert.isTrue(ClientModelMock.findOne.calledOnce);
                    assert.equal(client, expected);
                });
        });
    });
});

