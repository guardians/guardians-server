const sinon = require('sinon');
const assert = require('chai').assert;
const mongoose = require('mongoose');
const config = require('config');

require('../../models/Path');
const PathModel = mongoose.model('Path');

describe('PathModel', () => {
    describe('#findPath', () => {
        it('finds the latest version PathModel', () => {
            let client = {_id: '123456789'};
            let expected = {path: 'expected', client: client._id};
            mongoose.Model.findOne = sinon.stub()
                .resolves(expected);

            return PathModel.findPath('expected/path', client)
                .then(result => {
                    assert.deepEqual(result, expected);
                    assert.isTrue(mongoose.Model.findOne.calledOnce);
                    assert.deepEqual(mongoose.Model.findOne.firstCall.args, [{
                        $or: [
                            {path: 'expected/path'},
                            {path: {$regex: /^expected\/path\/*/}}
                        ],
                        client: client._id
                    }]);
                });
        });

        it('finds a specific version of PathModel', () => {
            let client = {_id: '123456789'};
            let expected = {path: 'expected', client: client._id};
            mongoose.Model.findOne = sinon.stub().resolves(expected);

            return PathModel.findPath('expected/path', client, 0)
                .then(result => {
                    assert.deepEqual(result, expected);
                    assert.isTrue(mongoose.Model.findOne.calledOnce);
                    assert.deepEqual(mongoose.Model.findOne.firstCall.args, [{
                        $or: [
                            {path: 'expected/path'},
                            {path: {$regex: /^expected\/path\/*/}}
                        ],
                        client: client._id,
                        'versions.number': 0
                    }]);
                });
        });
    });

    describe('#findFoldersContent', () => {
        it('finds the content of a folder', () => {
            let client = {_id: '123456789'};
            let expected = {path: 'expected', client: client._id};
            mongoose.Model.find = sinon.stub().resolves(expected);

            return PathModel.findRecursiveFolderContent('expected/path', client)
                .then(result => {
                    assert.deepEqual(result, expected);
                    assert.isTrue(mongoose.Model.find.calledOnce);
                    assert.deepEqual(mongoose.Model.find.firstCall.args, [
                        {
                            path: {$regex: /^expected\/path\/*/},
                            client: client._id,
                        }]);
                });
        });
    });

    describe('#deletePath', () => {
        it('deletes a path and it content', () => {
            let client = {_id: '123456789'};
            let expected = {path: 'expected', client: client._id};
            mongoose.Model.deleteMany = sinon.stub().resolves(expected);

            return PathModel.deletePath('expected/path', client)
                .then(result => {
                    assert.deepEqual(result, expected);
                    assert.isTrue(mongoose.Model.deleteMany.calledOnce);
                    assert.deepEqual(mongoose.Model.deleteMany.firstCall.args, [{
                        $or: [
                            {path: 'expected/path'},
                            {path: {$regex: /^expected\/path\/*/}}
                        ],
                        client: client._id,
                    }]);
                });
        });
    });

    describe('#getNextVersionNumber', () => {
        it('starts version number at 0', () => {
            let model = new PathModel({path: 'expected', client: '0123456789', versions: []});
            let versionNumber = model.getNextVersionNumber(model);
            assert.equal(versionNumber, 0);
        });

        it('increments the last version number', () => {
            let model = new PathModel({path: 'expected', client: '0123456789', versions: [{number: 4}, {number: 7}]});
            let versionNumber = model.getNextVersionNumber(model);
            assert.equal(versionNumber, 8);
        });
    });
});