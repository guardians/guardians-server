'use strict';
const winston = require('winston');
const ClientService = require('../services/Client');

module.exports = {
    login: (req, res, next) => {
        let username = req.header('username');
        let password = req.header('password');

        ClientService.login(username, password)
            .then(client => {
                req.client = client;
                next();
            })
            .catch(error => {
                winston.log('error', '401 - ' + error);
                res.status(401).send(error);
            });
    }
};

