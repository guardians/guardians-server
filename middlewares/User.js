'use strict';
const winston = require('winston');
const UserService = require('../services/User');

module.exports = {
    login: (req, res, next) => {
        let username = req.header('username');
        let password = req.header('password');
        let token = req.header('token');

        if (username && password) {
            return UserService.login(username, password)
                .then(user => {
                    req.user = user;
                    next();
                })
                .catch(error => {
                    winston.log('error', '401 - ' + error);
                    res.status(401).send(error);
                });
        }
        else if (token) {
            return UserService.loginUsingToken(token)
                .then(token => {
                    req.user = token.user;
                    next();
                })
                .catch(error => {
                    winston.log('error', '401 - ' + error);
                    res.status(401).send(error);
                });
        }

        let error = 'Please provide login/password or token';
        winston.log('error', '401 - ' + error);
        res.status(401).send(error);
    },

    loggedAsAdmin: (req, res, next) => {
        if (!(req.user && req.user.isAdmin)) {
            winston.log('error', '401 - forbidden: not logged as Admin User');
            return res.sendStatus(401);
        }

        return next();
    }
};

