# Guardians-Server
This project is part of the Guardians project, which is explained at [Guardians-Doc](https://framagit.org/guardians/guardians-doc).  
Guardians-Server stores every files sending by  [Guardians-Client](https://framagit.org/guardians/guardians-client), so your data is entirely safe in the server.

# How to install
```shell
git clone https://framagit.org/guardians/guardians-server # Clone the project
npm install # Install dependencies
npm run init # Configure Server; it will ask you few questions in order to work 
```

## A word about Server's initialization
First, server needs to have a SSL certificate in order to handle HTTPS requests. Please create a SSL certification before initialize the server.

When you start the `npm run init` command, it will ask few questions:
 * it configures server location and SSL certification location.
 * it configures mongo-db location.*
 * it offers you to generate a random salt.
This is the recommanded way to encrypt passwords. Use your own salt only if you have to reconfigure the server.
 * it offers you to create an Admin User.
Admin User is the only user which can create other Users. When you want to configure a new [Client](https://framagit.org/guardians/guardians-client), you have to know Admin login & password.

# Start the server
As soon as the server is configurated, you juste have to start it:
```shell
npm run start
```