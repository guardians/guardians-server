module.exports = {
    folders: {
        upload: '/tmp',
        latest: 'latest',
        versions: 'versions'
    },
    encryption: {
        key: 'test'
    },

    /*
    * HTTPS/SSL certificates
    */
    https: {
        key: "fakeSSLKey",
        cert: "fakeSSLCert"
    }
};