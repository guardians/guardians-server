const winston = require('winston');

module.exports = {
    /*
     * store data locations
     */
    folders: {
        upload: 'data',
        /*
         * the latest version of a file is located at folders.upload/folders.latest
         */
        latest: 'latest',

        /*
         * others versions of a file is located at folders.upload/folders.versions
         */
        versions: 'versions',
    },

    /*
     * Mongo connection url
     * Please see https://docs.mongodb.com/manual/reference/connection-string/
     */
    mongo: {
        url: process.env.MONGO_URL || 'mongodb://localhost:27017/GuardiansServer-dev'
    },

    /*
     * server port listening
     */
    port: process.env.PORT || 8000,

    /*
     * HTTPS/SSL certificates
     */
    // https: {
    //     key: fs.readFileSync('key.pem').toString(),
    //     cert: fs.readFileSync('cert.pem').toString()
    // },

    /*
     * Passwords encryptions
     *
     * Please provide `encryption.key` for each environment.
     */
    encryption: {
        method: 'sha256'
        // key: 'GuardiansServer'
    },

    /*
     * Path where every files are uploaded
     */
    // uploadFolder: /home/user/guardians

    /**
     * morgan logger options
     */
    logger: {
        access: {
            format: 'common',
            options: {}
        },
        winston: {
            transports: [
                new (winston.transports.File)({
                    filename: 'logs/errors.log',
                    level: 'info'
                })
            ]
        }
    },

    /**
     * Number of seconds a token should leave
     */
    token_time: 24 * 60 * 60
};

