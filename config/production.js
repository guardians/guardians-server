const fs = require('fs');

let generatedConfig = JSON.parse(fs.readFileSync('./config/production.json'));

module.exports = Object.assign(generatedConfig, {
    https: {
        key: fs.readFileSync(generatedConfig.https.keyPath).toString(),
        cert: fs.readFileSync(generatedConfig.https.certPath).toString()
    }
});
