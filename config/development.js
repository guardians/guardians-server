const fs = require('fs');
const winston = require('winston');

module.exports = {
    uploadFolder: './data',

    mongo: {
        url: "mongodb://localhost:27017/GuardiansServer-dev"
    },
    /**
     * morgan logger options
     */
    logger: {
        access: {
            format: 'dev',
            options: {}
        },
        winston: {
            transports: [
                new (winston.transports.Console)
            ]
        }
    },

    https: {
        key: fs.readFileSync('host.key').toString(),
        cert: fs.readFileSync('host.cert').toString()
    },
    encryption: {
        method: 'sha256',
        key: 'Guardians-Server'
    },

    /**
     * Number of seconds a token should leave
     */
    token_time: 60
};

