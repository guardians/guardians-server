const mongoose = require('mongoose');
const fs = require('q-io/fs');
const readlineSync = require('readline-sync');
const crypto = require("crypto");

const configOutput = './config/production.json';

let configOptions = {};

fs.exists(configOutput)
    .then(exists => {
        if (exists) {
            console.error(`Project has already been initialized. Please remove '${configOutput}' configurations`);
            process.exit(1);
        }
    })
    .then(() => fs.makeTree('./logs'))
    .then(createSSL)
    .then(createSalt)
    .then(createUploadFolder)
    .then(createMongoConfig)
    .then(createMongoConnection)
    .then(() => fs.write(configOutput, JSON.stringify(configOptions)))
    .then(createAdmin)
    .then(() => mongoose.disconnect())
    .then(() => console.log('DONE'))
    .catch(err => {
        console.log('error:', err);
        process.exit(1);
    });

// Set native promises as mongoose promise
mongoose.Promise = global.Promise;


function createAdmin() {
    console.log("\n\n # Administration");
    if (!readlineSync.keyInYN('Do you want to create Admin user ?')) {
        console.log('Warning: skipping Admin user creation');
        return Promise.resolve();
    }

    let adminUsername = readlineSync.question('Please provide Admin username: ');
    let adminPassword = readlineSync.questionNewPassword('Please provide Admin password: ', {
        min: 6,
        max: 128,
        mask: ''
    });

    let UserService = require('./services/User');
    return UserService.add(adminUsername, adminPassword, {isAdmin: true});
}

function createSSL() {
    console.log("\n\n # Manage SSL certificate");
    let keyPath = readlineSync.question("Please provide SSL key file path: ");
    let certPath = readlineSync.question("Please provide SSL cert file path: ");
    configOptions.https = {keyPath, certPath};
}

function createSalt() {
    console.log('\n\n # Create password salt');
    let key;

    if (readlineSync.keyInYN('Do you want to generate random password salt ?')) {
        key = crypto.randomBytes(64).toString('hex');
    } else {
        key = readlineSync.questionNewPassword("Please provide password salt:", {
            min: 16,
            max: 256,
            mask: ''
        });
    }

    configOptions.encryption = {key};
}

function createMongoConfig() {
    console.log("\n\n # DB Management");
    const url = readlineSync.question("Please provide MongoDB Url: ");
    configOptions.mongo = {url};
    return url;
}

function createMongoConnection(url) {
    mongoose.connect(url, (error) => {
        if (error) {
            console.error('Please make sure Mongodb is installed and running at ' + url);
            throw error;
        }
    });
}

function createUploadFolder() {
    console.log("\n\n Upload management");
    configOptions.folders = {
        upload: readlineSync.question("Where do you want to store clients' data: ")
    };
}