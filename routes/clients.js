const winston = require('winston');
let express = require('express');
let router = express.Router();

let ClientService = require('../services/Client');
let UserMiddleware = require('../middlewares/User');

// Manage User authentification
router.use(UserMiddleware.login);

function manageError (err, req, res) {
    let status, message;

    switch(err.code) {
        case 11000: // Duplicate key error
            status =  401;
            message = `${req.body.username} already exists`;
            break;
        default:
            status = 500;
            message = err.errmsg;
    }

    winston.log('error', status + ' - ' + message);
    res.status(status).send(message);
}

router.post('', (req, res) => {
    ClientService.add(req.body, req.user)
        .then(newClient => res.status(200).send(newClient))
        .catch(err => manageError(err, req, res));
});

router.put('', (req, res) => {
    ClientService.update(req.body, req.user)
        .then(newClient => res.status(200).send(newClient))
        .catch(err => manageError(err, res));
});

module.exports = router;
