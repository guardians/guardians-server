'use strict';
let winston = require('winston');
let express = require('express');
let router = express.Router();

let FindUserPathService = require('../services/Path/FindUserPathService');
let UserMiddleware = require('../middlewares/User');

// Manage User authentication
router.use(UserMiddleware.login);

function manageError(err, req, res) {
    let status = 500,
        message = err,
        code = null;
    if (err.code) {
        switch (err.code) {
            case 11000: // Duplicate key error
                status = 403;
                message = `${req.body.path} already exists`;
                code = "GS-EXISTS";
                break;
        }
    } else {
        code = err;

        switch (err) {
            case "GS-EXISTS":
                status = 403;
                message = `${req.body.path} already exists`;
                break;
            case "GS-PROPERTIES":
                status = 400;
                message = "Please provide acceptable parameters";
                break;
            case "GS-NORESULT":
                status = 400;
                message = `${req.body.path || req.params.path} doesn't exist`;
                break;
        }
    }

    winston.log('error', status + ' - ' + message);
    res.status(status).send({code, message});
}

router.get('/content/:client/:path', (req, res) => {
    let clientName = req.params.client;
    let pathToDownload = req.params.path;

    return FindUserPathService.findContent(pathToDownload, req.user, clientName)
        .then(path => res.status(200).send(path))
        .catch(err => manageError(err, req, res));
});

router.get('/download/:client/:path/:version?', (req, res) => {
    let clientName = req.params.client;
    let pathToDownload = req.params.path;
    let version = req.params.version;

    return FindUserPathService.download(pathToDownload, req.user, clientName, version)
        .then(path => res.status(200).send(path))
        .catch(err => manageError(err, req, res));
});

module.exports = router;
