'use strict';
let winston = require('winston');
let express = require('express');
let router = express.Router();

let FindPathService = require('../services/Path/FindPathService');
let AddPathService = require('../services/Path/AddPathService');
let UpdatePathService = require('../services/Path/UpdatePathService');
let RemovePathService = require('../services/Path/RemovePathService');
let VersionPathService = require('../services/Path/VersionPathService');

let ClientMiddleware = require('../middlewares/Client');

// Manage Client authentication
router.use(ClientMiddleware.login);

function manageError(err, req, res) {
    let status = 500,
        message = err,
        code = null;
    if (err.code) {
        switch (err.code) {
            case 11000: // Duplicate key error
                status = 403;
                message = `${req.body.path} already exists`;
                code = "GS-EXISTS";
                break;
        }
    } else {
        code = err;

        switch (err) {
            case "GS-EXISTS":
                status = 403;
                message = `${req.body.path} already exists`;
                break;
            case "GS-PROPERTIES":
                status = 400;
                message = "Please provide acceptable parameters";
                break;
            case "GS-NORESULT":
                status = 400;
                message = `${req.body.path || req.params.path} doesn't exist`;
                break;
        }
    }

    winston.log('error', status + ' - ' + message);
    res.status(status).send({code, message});
}

router.get('/content/:path', (req, res) => {
    let pathToDownload = req.params.path;

    return FindPathService.findContent(pathToDownload, req.client)
        .then(path => res.status(200).send(path))
        .catch(err => manageError(err, req, res));
});

router.get('/download/:path/:version?', (req, res) => {
    let pathToDownload = req.params.path;

    return FindPathService.download(pathToDownload, req.client)
        .then(response => {
            res.set('Content-disposition', 'attachment; filename=' + pathToDownload);
            res.set('Content-type', response.mimetype);
            response.content.pipe(res);
        })
        .catch(err => manageError(err, req, res));
});

router.put('', (req, res) => {
    let file = req.files ? req.files.file : undefined;
    return AddPathService.add(file, req.body.path, req.client)
        .then(path => res.status(200).send(path))
        .catch(err => manageError(err, req, res));
});

router.post('', (req, res) => {
    let file = req.files ? req.files.file : undefined;

    UpdatePathService.update(file, req.body.path, req.client)
        .then(path => res.status(200).send(path))
        .catch(err => manageError(err, req, res));
});

router.delete('', (req, res) => {
    return RemovePathService.remove(req.body.path, req.client)
        .then(path => res.status(200).send(path))
        .catch(err => manageError(err, req, res));
});

router.get('/download/:path/version/:version', (req, res) => {
    let pathToDownload = req.params.path;
    let versionToDownlad = req.params.version;

    return FindPathService.download(pathToDownload, req.client, versionToDownlad)
        .then(response => {
            res.set('Content-disposition', 'attachment; filename=' + pathToDownload);
            res.set('Content-type', response.mimetype);
            response.content.pipe(res);
        })
        .catch(err => manageError(err, req, res));
});

router.post('/version', (req, res) => {
    let file = req.files ? req.files.file : undefined;
    let keep = req.body.keep;
    VersionPathService.createVersion(file, req.body.path, req.client, keep)
        .then(path => res.status(200).send(path))
        .catch(err => manageError(err, req, res));
});

router.delete('/version', (req, res) => {
    let keepVersions = req.body.keep;
    return VersionPathService.removeVersions(req.body.path, req.client, keepVersions)
        .then(path => res.status(200).send(path))
        .catch(err => manageError(err, req, res));
});

module.exports = router;
