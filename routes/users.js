const winston = require('winston');
let express = require('express');
let router = express.Router();

let UserService = require('../services/User');
let ClientService = require('../services/Client');
let UserMiddleware = require('../middlewares/User');

// Manage User authentication
router.use(UserMiddleware.login);

function manageError(err, req, res) {
    let status, message;

    switch (err.code) {
        case 11000: // Duplicate key error
            status = 401;
            message = `${req.body.username} already exists`;
            break;
        default:
            status = 500;
            message = err.errmsg;
    }

    winston.log('error', status + ' - ' + message);
    res.status(status).send(message);
}

router.get('/clients', (req, res) => {
    return ClientService.list(req.user)
        .then(clients => res.status(200).send(clients))
        .catch(err => manageError(err, req, res));
});

router.get('/login', (req, res) => {
    return UserService.createToken(req.user)
        .then(user => res.status(200).send(user));
});

router.post('', UserMiddleware.loggedAsAdmin, (req, res) => {
    UserService.add(req.body.username, req.body.password)
        .then(newUser => res.status(200).send(newUser))
        .catch(err => manageError(err, req, res));
});

router.put('', (req, res) => {
    UserService.update(req.body, req.user)
        .then(newUser => res.status(200).send(newUser))
        .catch(err => manageError(err, res));
});

module.exports = router;
