'use strict';
const mongoose = require('mongoose');
let utils = require('../utils');

let UserModel = mongoose.model('User');
let UserTokenModel = mongoose.model('UserToken');

class UserService {
    static add(username, password, options = {}) {
        if (!(username && password)) {
            return Promise.reject('User: User name or password not provided');
        }

        let userValues = Object.assign(options, {
            username: username,
            password: utils.salt(password)
        });

        let user = new UserModel(userValues);
        return user.save();
    }

    static login(username, password) {
        if (!username || !password) {
            return Promise.reject('Please provide username/password');
        }

        password = utils.salt(password);
        return UserModel.findOne({username, password})
            .then(user => {
                if (!user) {
                    throw 'Bad User username/password';
                }

                return user;
            });
    }

    static loginUsingToken(token) {
        if (!token) {
            return Promise.reject('Please provide token');
        }

        return UserTokenModel.findByToken(token)
            .then(token => {
                if (!token) {
                    throw 'Bad User token';
                }

                return token;
            });
    }

    static createToken(user) {
        return UserTokenModel.createToken(user)
            .then(token => UserTokenModel.findByToken(token.token));
    }
}

module.exports = UserService;
