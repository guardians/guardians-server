'use strict';
let nodeFS = require('fs');
let fs = require('q-io/fs');
let path = require('path');
let targz = require('tar.gz');
let mongoose = require('mongoose');
let FSPathService = require("./FSPathService");

let PathModel = mongoose.model('Path');

class FindPathService {

    /**
     * Finds direct content of a specific folder path
     *
     * @param folderPath {string} - the folder to find content
     * @param client {Client} - the owner of the folder
     * @return {Promise}
     */
    static findContent(folderPath, client) {
        if (!client) {
            return Promise.reject('Path: file properties or Client are missing');
        }

        if (!folderPath) {
            folderPath = '/';
        }
        else {
            if (!folderPath.startsWith(path.sep)) {
                folderPath = path.sep + folderPath
            }

            if (!folderPath.endsWith(path.sep)) {
                folderPath += path.sep;
            }
        }

        return PathModel.findDirectFolderContent(folderPath, client);
    }

    /**
     * Find a path, and create a readable stream containing the content to download
     *
     * @param filepath string - the path to download
     * @param client ClientModel - the owner of the path
     * @param version Number - the version of the file to download
     * @return Promise - a Promise which is resolved as soon as the stream is ready
     */
    static download(filepath, client, version = null) {
        if (!(filepath && client)) {
            return Promise.reject('Path: file properties or Client are missing');
        }

        let pathToDownload = FSPathService.getPath(filepath, client, version);

        return PathModel.findPath(filepath, client, version)
            .then(found => {
                if (found === null) {
                    return Promise.reject('GS-NORESULT');
                }

                return FindPathService.createDownloadableStream(pathToDownload, found);
            });
    }

    /**
     * Create a Stream from a path
     * If the path points to a folder, tar.gz this folder.
     *
     * @private
     * @param filepath {string} - the file/folder to turn into a ReadableStream
     * @param options {Object} - options to add into the response
     * @return {Promise}
     */
    static createDownloadableStream(filepath, options) {
        return fs.isDirectory(filepath)
            .then(isDirectory => {
                let content,
                    mimetype;

                if (isDirectory) {
                    content = targz({}, {
                        fromBase: true
                    }).createReadStream(filepath);
                    mimetype = "application/tar+gzip";
                } else {
                    mimetype = options.mimetype;
                    content = nodeFS.createReadStream(filepath);
                }

                return {
                    path: filepath,
                    mimetype,
                    content
                };
            });
    }
}

module.exports = FindPathService;