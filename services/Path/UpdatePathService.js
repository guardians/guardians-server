let fs = require('q-io/fs');
let mongoose = require('mongoose');
let FSPathService = require('./FSPathService');

let PathModel = mongoose.model('Path');

class UpdatePathService {

    /**
     * Update an existing file.
     *
     * The returned Promise if rejected if the Client/filepath doesn't exist in DB,
     * and resolved as soon as FS made the change.
     *
     * @param file {express}-fileupload - the uploaded file
     * @param filepath {string} - the path of the uploaded file, included filename
     * @param client ClientModel - the client who send the file
     * @return {Promise} - resolved as soon as the file is correctly updated in FS and in DB
     */
    static update(file, filepath, client) {
        if (!(filepath && client)) {
            return Promise.reject('GS-PROPERTIES');
        }

        return PathModel.findOne({client: client._id, path: filepath})
            .then(pathModel => {
                if (!pathModel) {
                    throw 'GS-NORESULT';
                }

                return fs.write(FSPathService.getPath(filepath, client), file ? file.data : '')
                    .then(() => pathModel);
            });
    }
}

module.exports = UpdatePathService;