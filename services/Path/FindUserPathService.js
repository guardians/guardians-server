'use strict';

let mongoose = require('mongoose');
let FindPathService = require('./FindPathService');
let ClientModel = mongoose.model('Client');

class FindUserPathService {

    /**
     * Finds direct content of a specific folder path
     *
     * @param folderPath {string} - the folder to find content
     * @param user {User} - the owner of the client
     * @param clientName {string} - the client name
     * @return {Promise}
     */
    static findContent(folderPath, user, clientName) {
        if (!(folderPath && user && clientName)) {
            return Promise.reject('Path: file properties, User or Client are missing');
        }

        return ClientModel.findByName(user, clientName)
            .then(client => FindPathService.findContent(folderPath, client));
    }

    /**
     * Find a path, and create a readable stream containing the content to download
     *
     * @param filepath string - the path to download
     * @param user {User} - the owner of the client
     * @param clientName {string} - the client name
     * @param version Number - the version of the file to download
     * @return Promise - a Promise which is resolved as soon as the stream is ready
     */
    static download(filepath, user, clientName, version = null) {
        if (!(filepath && user && clientName)) {
            return Promise.reject('Path: file properties, User or Client are missing');
        }

        return ClientModel.findByName(user, clientName)
            .then(client => FindPathService.download(filepath, client, version));
    }
}

module.exports = FindUserPathService;