'use strict';

let path = require('path');
let fs = require('q-io/fs');
let mongoose = require('mongoose');
let FSPathService = require('./FSPathService');

let PathModel = mongoose.model('Path');

class AddPathService {
    /**
     * Add a new file.
     * Save this new file into `${config.uploadFolder}/filepath` ;
     * add this new path into DB in order to retrieve it later.
     *
     * The returned promise is rejected if there is no other file for this Client/filepath,
     * and resolved as soon as both FS and DB made the changes.
     *
     * @param file fileupload - the uploaded file
     * @param filepath {string} - the path of the uploaded file, included filename
     * @param client PathModel - the client who send the file
     * @return Promise - resolved as soon as the file is correctly added in FS and in DB
     */
    static add(file, filepath, client) {
        if (!(filepath && client)) {
            return Promise.reject('Path: file properties or Client are missing');
        }

        let pathModel = new PathModel({
            client: client._id,
            path: filepath,
            mimetype: file ? file.mimetype : 'file'
        });

        let pathToSave = FSPathService.getPath(filepath, client);
        return pathModel.save()
            .then(savedPath => {
                pathModel = savedPath;
            })
            .then(() => this.addDirectories(filepath, client))
            .then(() => fs.makeTree(path.dirname(pathToSave)))
            .then(() => {
                if (!file) {
                    return Promise.resolve();
                }
                return fs.write(pathToSave, file.data)
            })
            .then(() => pathModel);
    }

    static addDirectories(filepath, client) {
        let allPromises = [];

        let splittedPath = filepath.split(path.sep);
        splittedPath = splittedPath.slice(0, splittedPath.length - 1);

        if (splittedPath[0] === "") {
            splittedPath[0] = path.sep;
        }

        splittedPath.reduce((acc, currentPath) => {

            let next;

            if (acc === "") {
                next = currentPath;
            }
            else if (acc === path.sep) {
                next = acc + currentPath;
            }
            else {
                next = acc + path.sep + currentPath;
            }

            allPromises.push(PathModel.addDirectoryPath(next, client));

            return next;
        }, '');

        return Promise.all(allPromises);
    }
}

module.exports = AddPathService;