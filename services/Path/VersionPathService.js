'use strict';

let path = require('path');
let fs = require('q-io/fs');
let mongoose = require('mongoose');

let FSPathService = require("./FSPathService");

let PathModel = mongoose.model('Path');

class VersionPathService {

    /**
     * Create a new version of an existing file
     * If `keep` is provided, only keep `keep` versions, including the new one to create.
     *
     * @param file fileupload - the uploaded file
     * @param filepath {string} - the path of the uploaded file, included filename
     * @param client PathModel - the client who send the file
     * @param keep {number|null} - number of versions to keep
     * @return Promise - resolved as soon as the new version is correctly added in FS and in DB
     */
    static createVersion(file, filepath, client, keep = null) {
        if (!(file && filepath && client)) {
            return Promise.reject('GS-PROPERTIES');
        }

        return PathModel.findPath(filepath, client)
            .then(pathModel => {
                if (!pathModel) {
                    throw 'GS-NORESULT';
                }

                const nextVersionNumber = pathModel.getNextVersionNumber();
                pathModel.versions.push({
                    number: nextVersionNumber,
                    date: pathModel.date
                });
                const versionPath = FSPathService.getPath(filepath, client, nextVersionNumber);
                const latestPath = FSPathService.getPath(filepath, client);

                return VersionPathService.manageKeepVersions(pathModel, client, keep)
                    .then(() => fs.makeTree(path.dirname(versionPath)))
                    .then(() => fs.move(latestPath, versionPath))
                    .then(() => fs.write(latestPath, file.data))
                    .then(() => pathModel);
            });
    }

    /**
     * Manage the number of versions to keep for the provided file.
     * If `keep` is provided, remove every versions of the provided file, except the `keep` last versions.
     * Save the pathModel changes.
     *
     * @param pathModel {PathModel} - the pathModel to manage
     * @param client {ClientModel} - the owner of the pathModel
     * @param keep {number|null} - the number of versions to keep ; if it isn't provided, just save the pathModel as is.
     * @return {Promise}
     */
    static manageKeepVersions(pathModel, client, keep) {
        if (keep) {
            return VersionPathService.deleteVersions(pathModel, client, keep)
                .then(pathModel => pathModel.save());
        }

        return pathModel.save();
    }

    /**
     * Delete all versions of the provided pathModel
     *
     * @param filepath {string} - the path of the file we have to delete its versions
     * @param client {ClientModel} - the owner of the file
     * @param keep {number|null} - number of versions to keep.
     */
    static removeVersions(filepath, client, keep = null) {
        if (!(filepath && client)) {
            return Promise.reject('GS-PROPERTIES');
        }

        return PathModel.findPath(filepath, client)
            .then(pathModel => {
                if (!pathModel) {
                    throw 'GS-NORESULT';
                }

                return VersionPathService.deleteVersions(pathModel, client, keep)
                    .then(pathModel => pathModel.save());
            });
    }

    /**
     * Delete all versions of the provided pathModel
     * This should ONLY be used if you're sure the provided pathModel has been loaded,
     * and the pathModel is owned by the provided client.
     *
     * Change the pathModel by removing every deleted versions,
     * but don't save the pathMoel in DB: you have to save it yourself if you want to persist changes.
     *
     *
     * @param pathModel - the path to delete
     * @param client - the owner of the path
     * @param keep {number|null} - number of versions to keep
     * @return {Promise}
     */
    static deleteVersions(pathModel, client, keep = null) {
        // If there is no version stored, do nothing
        if (!(pathModel.versions && pathModel.versions.length)) {
            return Promise.resolve(pathModel);
        }

        // Compute which versions we have to keep, and which we have to delete,
        // depending on `keep` parameter
        let versionsToDelete,
            versionsToKeep;
        if (keep) {
            if (pathModel.versions.length <= keep) {
                return Promise.resolve(pathModel);
            }

            versionsToDelete = pathModel.versions.slice(0, pathModel.versions.length - keep);
            versionsToKeep = pathModel.versions.slice(pathModel.versions.length - keep, pathModel.versions.length);
        } else {
            versionsToDelete = pathModel.versions;
            versionsToKeep = [];
        }

        let allPromises = versionsToDelete.map(version => {
            return fs.remove(FSPathService.getPath(pathModel.path, client, version.number));
        });

        return Promise.all(allPromises)
            .then(() => {
                pathModel.versions = versionsToKeep;
                return pathModel;
            });
    }

    /**
     * Delete all versions of the provided pathModel
     *
     * @param pathModelArray - An array of paths to delete
     * @param client - the owner of paths
     * @return {Promise}
     */
    static deleteAllFilesVersions(pathModelArray, client) {
        let allPromises = pathModelArray.map(pathModel => {
            return this.deleteVersions(pathModel, client, null)
                .then(pathModel => pathModel.save());
        });
        return Promise.all(allPromises);
    }
}

module.exports = VersionPathService;