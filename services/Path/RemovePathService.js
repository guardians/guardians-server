'use strict';

let fs = require('q-io/fs');
let mongoose = require('mongoose');
let FSPathService = require('./FSPathService');
let VersionPathService = require('./VersionPathService');

let PathModel = mongoose.model('Path');

class RemovePathService {

    /**
     * Delete an existing path.
     * If provided filepath refers to a folder, remove this folder and all its childs recursively.
     *
     * @param filepath {string} - the path to remove
     * @param client ClientModel - the owner of the path to remove
     * @return {Promise} - resolved as soon as the path is deleted ; rejected if the provided path doesn't exist for the provided client.
     */
    static remove(filepath, client) {
        if (!(filepath && client)) {
            return Promise.reject('GS-PROPERTIES');
        }

        let pathToDelete = FSPathService.getPath(filepath, client);
        return PathModel.findPath(filepath, client)
            .then(pathModel => {
                if (!pathModel) {
                    throw 'GS-NORESULT';
                }

                return fs.isDirectory(pathToDelete)
                    .then(isDirectory => isDirectory ?
                        RemovePathService.deleteFolder(filepath, client) :
                        RemovePathService.deleteFile(pathModel, client));
            })
            .then(result => {
                if (!result || result.deletedCount === 0) {
                    throw 'GS-NORESULT';
                }

                return fs.removeTree(pathToDelete)
                    .then(() => result)
                    .catch(() => {
                        throw 'GS-NORESULT';
                    });
            });
    }

    /**
     * Remove a folder, and its childs recursively from DB
     *
     * @private
     *
     * @param client ClientModel - the owner of the filepath
     * @param filepath {string} - the filepath
     * @return {Promise} - resolved as soon as the folder is deleted from DB.
     */
    static deleteFolder(filepath, client) {
        return PathModel.findRecursiveFolderContent(filepath, client)
            .then(foldersPath => {
                return VersionPathService.deleteAllFilesVersions(foldersPath, client);
            })
            .then(() => PathModel.deletePath(filepath, client));
    }

    /**
     * Deletes a file from DB
     * Deletes the file and all these versions from FS
     *
     * @private
     *
     * @param pathToDelete {PathModel} - the PathModel to delete
     * @param client ClientModel - the owner of the filepath
     * @return {Promise} - resolved as soon as the file is deleted from DB.
     */
    static deleteFile(pathToDelete, client) {
        return VersionPathService.deleteVersions(pathToDelete, client, null)
            .then(() => PathModel.deletePath(pathToDelete.path, client));
    }
}

module.exports = RemovePathService;