const path = require('path');
const config = require('config');

class FSPathService {
    /**
     * Find the absolute path for a client and a filepath
     *
     * @param filepath {string} - the filepath
     * @param client ClientModel - the owner of the filepath
     * @param version {number|null} - the version of the file to access
     * @return {string} the absolute path
     */
    static getPath(filepath, client, version = null) {
        let joined = [
            config.get('folders.upload')
        ];

        if (version !== null) {
            joined.push(
                config.get('folders.versions'),
                version.toString()
            );
        } else {
            joined.push(config.get('folders.latest'));
        }

        joined.push(
            client.username,
            filepath
        );

        return path.normalize(
            path.join(...joined)
        );
    }
}

module.exports = FSPathService;