'use strict';

const mongoose = require('mongoose');
let utils = require('../utils');

let ClientModel = mongoose.model('Client');

class ClientService {
    static list(user) {
        if (!user) {
            return Promise.reject('Client: User is not provided');
        }

        return ClientModel.findByUser(user);
    }

    static add(clientValues, user) {
        if (!(clientValues.username && clientValues.password)) {
            return Promise.reject('Client: Client name or password not provided');
        }

        if (!user) {
            return Promise.reject('Client: User not provided');
        }

        clientValues.password = utils.salt(clientValues.password);
        clientValues.user = user._id;

        let client = new ClientModel(clientValues);
        return client.save();
    }

    static update(values, user) {
        if (!(values.username && values.password)) {
            return Promise.reject('Client: Client name or password not provided');
        }

        return ClientModel.findOne({username: values.username})
            .then(client => {
                if (!client) {
                    throw "This Client doesn't exist";
                }

                values.password = utils.salt(values.password);
                return client.update(values);
            });
    }

    static login(username, password) {
        if (!username || !password) {
            return Promise.reject('Please provide username/password');
        }

        password = utils.salt(password);
        return ClientModel.findOne({username, password})
            .then(client => {
                if (!client) {
                    return Promise.reject('Bad Client username/password');
                }

                return client;
            });
    }
}

module.exports = ClientService;
