const express = require('express');
const path = require('path');
const logger = require('morgan');
const bodyParser = require('body-parser');
const fileUpload = require('express-fileupload');
const config = require('config');
const winston = require('winston');

// Load every Mongoose Models
require('./models/Client');
require('./models/User');
require('./models/UserToken');
require('./models/Path');

const cors = require('cors');
const users = require('./routes/users');
const clients = require('./routes/clients');
const paths = require('./routes/clientPaths');
const userPaths = require('./routes/userPaths');
const availability = require('./routes/availability');
const app = express();

winston.configure({
    transports: config.get('logger.winston.transports')
});

app.use(logger(config.get('logger.access.format'), config.get('logger.access.options')));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(fileUpload());

app.options('*', cors());

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.use('/availability', availability);
app.use('/users', users);
app.use('/clients', clients);
app.use('/paths', paths);
app.use('/userpaths', userPaths);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    let err = new Error('Not Found');
    err.status = 404;
    winston.log('error', req.path + ' - 404 NOT FOUND');

    next(err);
});

// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    let statusCode = err.status || 500;

    winston.log('error', statusCode + ' - ' + res.locals.message);
    res.status(statusCode);
    res.json(res.locals.message);
});

module.exports = app;
