'use strict';

const config = require('config');
const mongoose = require('mongoose');
const randTokenUid = require('rand-token').uid;

const tokenTime = config.get('token_time') * 1000;
const Schema = mongoose.Schema;

let userTokenSchema = mongoose.Schema({
    expires: {
        type: Date,
        required: true,
        expires: tokenTime,
    },
    token: {
        type: String,
        required: true
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    }
});

userTokenSchema.statics.createToken = function (user) {
    let expires = new Date();
    expires.setTime(expires.getTime() + tokenTime);

    let UserToken = mongoose.model('UserToken');
    let token = new UserToken({
        expires,
        token: randTokenUid(16),
        user: user._id
    });

    return token.save();
};

userTokenSchema.statics.findByToken = function (token) {
    return mongoose.model('UserToken').findOne({token})
        .populate('user');
};

module.exports = mongoose.model('UserToken', userTokenSchema);
