const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const clientSchema = new Schema({
    username: {type: String, required: true, unique: true},
    password: {type: String, required: true},
    user: {type: mongoose.Schema.Types.ObjectId, ref: 'User'}
});

clientSchema.statics.findByUser = function (user) {
    return mongoose.model('Client').find(
        {user: user._id.toString()}
    );
};

clientSchema.statics.findByName = function (user, username) {
    return mongoose.model('Client').findOne(
        {
            user: user._id.toString(),
            username: username
        }
    );
};
const Client = mongoose.model('Client', clientSchema);


module.exports = Client;
