let mongoose = require('mongoose');
let Schema = mongoose.Schema;
let path = require('path');

// Make sure regexp-escape is loaded
require('regexp-escape');

let pathSchema = new Schema({
    client: {
        type: mongoose.Schema.Types.ObjectId, ref: 'Client',
        required: true
    },
    path: {
        type: String,
        required: true
    },
    mimetype: String,
    date: Date,
    versions: [{
        number: Number,
        date: Date
    }]
});
pathSchema.index({client: 1, path: 1}, {unique: true});

pathSchema.pre('save', function (next) {
    this.date = new Date();
    next();
});

pathSchema.methods.getNextVersionNumber = function () {
    if (this.versions.length === 0) {
        return 0;
    }

    return this.versions[this.versions.length - 1].number + 1;
};

const filesAndFoldersForClientQuery = function (filepath, client) {
    return {
        $or: [
            {path: filepath},
            {path: {$regex: folderPathRegex(filepath)}}
        ],
        client: client._id
    };
};

const folderPathRegex = function (folderPath) {
    let normalizedFolderPath = path.normalize(folderPath + path.sep);
    return new RegExp('^' + RegExp.escape(normalizedFolderPath) + '*');
};

pathSchema.statics.findPath = function (filepath, client, version = null) {
    const query = filesAndFoldersForClientQuery(filepath, client);

    if (version !== null) {
        query["versions.number"] = version;
    }

    return this.model('Path').findOne(query);
};

pathSchema.statics.findDirectFolderContent = function (folderPath, client) {
    let regexPath = '^' + RegExp.escape(folderPath) + '[A-Za-z0-9_\\-\\.\\ ]+$';

    return this.model('Path').find({
        path: {$regex: regexPath},
        client: client._id.toString()
    });
};

pathSchema.statics.findRecursiveFolderContent = function (folderPath, client) {
    let regexPath = folderPathRegex(folderPath);

    return this.model('Path').find({
        path: {$regex: regexPath},
        client: client._id
    });
};

pathSchema.statics.addDirectoryPath = function (directoryPath, client) {
    let model = this.model('Path');
    return model.findOne({
        path: directoryPath,
        client: client._id
    })
        .then(path => {
            if (path) {
                return Promise.resolve();
            }

            return new model({
                path: directoryPath,
                client: client,
                mimetype: 'folder'
            })
                .save();
        })
};

pathSchema.statics.deletePath = function (filepath, client) {
    return this.model('Path').deleteMany(filesAndFoldersForClientQuery(filepath, client));
};

module.exports = mongoose.model('Path', pathSchema);
