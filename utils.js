const crypto = require('crypto');
const config = require('config');

const encryptionMethod = config.get('encryption.method');
const encryptionKey = config.get('encryption.key');

module.exports = {
    salt: value => {
        return crypto
            .createHmac(encryptionMethod, encryptionKey)
            .update(value)
            .digest('hex');
    }
};

